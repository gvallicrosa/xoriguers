#!/usr/bin/env python3

# Override imports
import sys
path = [name for name in sys.path if '2.7' not in name]
print(path)
sys.path = path

import matplotlib.pyplot as plt
import numpy as np

from collections import defaultdict

import seaborn as sns
sns.set()
sns.set_color_codes()


colles = ('arreplega',
          'bergan',
          'desconegu',
          'emboir',
          'engresca',
          'ganapi',
          'ganàpi',
          'grilla',
          'llunatic',
          'marrac',
          'passarell',
          'passerell',
          'pataquer',
          'penja',
          'trempa',
          'marrec')


class TwoCounters(object):

    def __init__(self):
        self.inside = 0
        self.outside = 0

    def __getitem__(self, key):
        if key in (0, 1):
            if key == 0:
                return self.inside
            else:
                return self.outside
        else:
            raise Exception('Invalid key {}', key)

    def __setitem__(self, key, value):
        if key in (0, 1):
            if key == 0:
                self.inside = value
            else:
                self.outside = value
        else:
            raise Exception('Invalid key {}', key)

    def __repr__(self):
        return "[{:d}, {:d}]".format(self.inside, self.outside)

    def __str__(self):
        return "[{:d}, {:d}]".format(self.inside, self.outside)


def count_dotfile(fname, count):
    """Count with how many people provided a .dot file."""
    assert(fname.endswith('.dot'))
    # Helper to number outsiders
    outsiders = list()
    # Graph DOT file
    fh = open(fname)
    line = fh.readline()
    while line:
        # Is a node link
        if '--' in line:
            # Get names
            try:
                temp = line.split()
                name1 = temp[0].lower()
                name2 = temp[2].lower()
            except:
                print("Problem with line: {:s}".format(line))
                line = fh.readline()
                continue
            # Only name2 can be from outside xoriguers
            outside = False
            for colla in colles:
                if name2.startswith(colla):
                    count[name1][1] += 1
                    outside = True
                    outsiders.append(name2)
                    break
            if not outside:
                # Between people in the colla
                count[name1][0] += 1
                count[name2][0] += 1
        # Next iteration
        line = fh.readline()
    # Close
    fh.close()
    # Outsiders
    outsiders.sort()
    print("# Outsiders")
    for outsider in outsiders:
        print(outsider)
    print("# Outsiders end")
    # Return
    return count


def compute_color(values):
    color = [0, ] * len(values)
    curr = 0
    one = 105 / 255.
    two = 169 / 255.
    VALS = [(one, one, one, 1.0), (two, two, two, 1.0)]
    last = values[0]
    for i in range(len(values)):
        if values[i] != last:
            last = values[i]
            if curr == 0:
                curr = 1
            else:
                curr = 0
        color[i] = VALS[curr]
    return color


def plot_counts(count):
    # to check names
    names = list(count.keys())
    names.sort()
    for name in names:
        print(name)
    # individual counts
    inside = [(count[key][0], key) for key in count]
    outside = [(count[key][1], key) for key in count]
    total = [(count[key][0] + count[key][1], key) for key in count]
    # sort from biggest
    inside.sort(reverse=True)
    outside.sort(reverse=True)
    total.sort(reverse=True)
    # print values
    print("# dins la colla")
    for i in range(5):
        print("{:2d} {:s}".format(inside[i][0], inside[i][1]))
    print("\n# fora la colla")
    for i in range(5):
        print("{:2d} {:s}".format(outside[i][0], outside[i][1]))
    print("\n# total")
    for i in range(5):
        print("{:2d} {:s}".format(total[i][0], total[i][1]))

    size = (16, 4)

    # plot inside
    value = [v[0] for v in inside if v[0] > 0]
    names = [v[1] for v in inside if v[0] > 0]
    poses = np.arange(len(value)) + 0.5
    color = compute_color(value)
    fig, ax = plt.subplots(1, 1, figsize=size)
    plt.xticks(rotation=90)
    ax.bar(poses, value, width=1.0, align='center',
           tick_label=names, color=color)
    fig.tight_layout()
    fig.savefig('dins.png')
    fig.savefig('dins.svg')
    fig.savefig('dins.pdf')

    # plot outside
    value = [v[0] for v in outside if v[0] > 0]
    names = [v[1] for v in outside if v[0] > 0]
    poses = np.arange(len(value)) + 0.5
    color = compute_color(value)
    fig, ax = plt.subplots(1, 1, figsize=size)
    plt.xticks(rotation=90)
    ax.set_yticks(range(value[0] + 1))
    ax.bar(poses, value, width=1.0, align='center',
           tick_label=names, color=color)
    fig.tight_layout()
    fig.savefig('fora.png')
    fig.savefig('fora.svg')
    fig.savefig('fora.pdf')
    # plt.show()


if __name__ == '__main__':
    # Count from all files
    count = defaultdict(TwoCounters)  # inside, outside
    for fname in sys.argv[1:]:
        count = count_dotfile(fname, count)
    # Plot something
    plot_counts(count)
