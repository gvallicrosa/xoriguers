#!/usr/bin/env python3

"""
Script to convert from .dot file to a .dia file for easy manipulation.

The Graphviz package is used to generate an intermediate .svg representation
to easily extract the position of each element. Then the relationships defined
in the .dot file are used to create the .dia file.

The .dot file has to be defined following this example:

graph malla {
// Some configs
ratio="0.7";

{  // First group
node [shape=ellipse, penwidth=2, style=rounded, color=red]
hola
}
{  // Second group
node [shape=rectangle, color=blue]
que
}K
// Relationships
hola -- que
}
"""

import sys
import os
import numpy as np
from collections import defaultdict
from xml.dom import minidom

# Outside name starts
COLLES = ('arreplega',
          'bergan',
          'desconegu',
          'emboir',
          'engresca'
          'ganapia',
          'ganàpia',
          'grilla',
          'llunatic',
          'marrac',
          'passarell',
          'passerell',
          'pataquer',
          'penja',
          'trempa')

# Dia preamble tempplate
PREAMBLE = (
    '<?xml version="1.0" encoding="UTF-8"?>\n'
    '<dia:diagram xmlns:dia="http://www.lysator.liu.se/~alla/dia/">\n'
    '  <dia:layer name="Background" visible="true" active="true">\n\n')

# Rectangle string template (id)
# obj_pos == elem_corner
RECTANGLE = (
    '    <dia:object type="Standard - Box" version="0" id="0{:d}">\n')
# Ellipse string template (id, x, y, x, y, w, h)
ELLIPSE = (
    '    <dia:object type="Standard - Ellipse" version="0" id="0{:d}">\n')

# Rectangle/Ellipse positioning (x, y, x, y, w, h, style)
STYLES = [0, 4]  # continuous, dashed
POSITION = (
    '      <dia:attribute name="obj_pos">\n'
    '        <dia:point val="{:.2f},{:.2f}"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="elem_corner">\n'
    '        <dia:point val="{:.2f},{:.2f}"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="elem_width">\n'
    '        <dia:real val="{:.2f}"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="elem_height">\n'
    '        <dia:real val="{:.2f}"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="border_width">\n'
    '        <dia:real val="0.25"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="show_background">\n'
    '        <dia:boolean val="true"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="line_style">\n'
    '        <dia:enum val="{:d}"/>'
    '      </dia:attribute>'
    '    </dia:object>\n\n')

# Text string template (id, x, y, text, toid)
TEXT = (
    '    <dia:object type="Standard - Text" version="1" id="0{}">\n'
    '      <dia:attribute name="obj_pos">\n'
    '        <dia:point val="{:.2f},{:.2f}"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="text">\n'
    '        <dia:composite type="text">\n'
    '          <dia:attribute name="string">\n'
    '            <dia:string>#{:s}#</dia:string>\n'
    '          </dia:attribute>\n'
    '          <dia:attribute name="font">\n'
    '            <dia:font family="sans" style="0" name="Helvetica"/>\n'
    '          </dia:attribute>\n'
    '          <dia:attribute name="height">\n'
    '            <dia:real val="0.8"/>\n'
    '          </dia:attribute>\n'
    '          <dia:attribute name="color">\n'
    '            <dia:color val="#000000"/>\n'
    '          </dia:attribute>\n'
    '          <dia:attribute name="alignment">\n'
    '            <dia:enum val="1"/>\n'
    '          </dia:attribute>\n'
    '        </dia:composite>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="valign">\n'
    '        <dia:enum val="3"/>\n'
    '      </dia:attribute>\n'
    '      <dia:connections>\n'
    '        <dia:connection handle="0" to="0{}" connection="8"/>\n'
    '      </dia:connections>\n'
    '    </dia:object>\n\n')

# Arrow string template (id, fromid, toid)
ARROW = (
    '    <dia:object type="Standard - Line" version="0" id="0{:d}">\n'
    '      <dia:attribute name="numcp">\n'
    '        <dia:int val="1"/>\n'
    '      </dia:attribute>\n'
    '      <dia:attribute name="line_width">\n'
    '        <dia:real val="0.25"/>\n'
    '      </dia:attribute>\n'
    '      <dia:connections>\n'
    '        <dia:connection handle="0" to="0{:d}" connection="8"/>\n'
    '        <dia:connection handle="1" to="0{:d}" connection="8"/>\n'
    '      </dia:connections>\n'
    '    </dia:object>\n\n')


# ==============================================================================
def graph_from_dot(fname):
    """
    Create an undirected graph provided a .dot file.

    It relies on all lines containing 'name1 -- name2'
    """
    # Graph DOT file
    fh = open(fname)
    line = fh.readline()
    graph = defaultdict(list)
    linenum = 0
    while line:
        # Is a node link
        if '--' in line:
            try:
                # Get names
                temp = line.split()
                name1 = temp[0]
                name2 = temp[2]
                # Save graph
                graph[name1].append(name2)
            except:
                print("graph error in line[{:d}]: {:s}".format(linenum, line))
        # Next iteration
        line = fh.readline()
        linenum += 1
    # Close
    fh.close()
    # Return
    return graph


# ==============================================================================
def names_from_dot(fname):
        """
        Get all names from a .dot file.

        It relies on all lines not containing strange characters.
        """
        # Graph DOT file
        names = list()
        fh = open(fname)
        line = fh.readline()
        while line:
            # Check len after splitting
            temp = line.split()
            if len(temp) == 1:
                if '/' not in line:
                    if temp[0].isalnum():
                        names.append(temp[0])
            # Next iteration
            line = fh.readline()
        # Close
        fh.close()
        # Return
        names.sort()
        return names


# ==============================================================================
def check_names(graph, names):
    """
    Checks unused or undefined names.

    Return false on any finding.
    """
    # Gather all names from graph
    allnames = list()
    for key in graph:
        if key not in allnames:
            allnames.append(key)
        for name in graph[key]:
            if name not in allnames:
                allnames.append(name)
    # Look for undefined ones
    undefined = 0
    for name in allnames:
        if name not in names:
            print("!! undefined node '{:s}'".format(name))
            undefined += 1
    # Look for unused ones
    unused = 0
    for name in names:
        if name not in allnames:
            print("!! unused node '{:s}', delete it".format(name))
            unused += 1
    # Return check
    if undefined == 0 and unused == 0:
        return True
    return False


# ==============================================================================
def dia_creator(name, graph):
    """Create a dia file from a svg and a graph of dependencies."""
    # Init
    idx = 0
    diafh = open(name + '.dia', 'w')
    diafh.write(PREAMBLE)
    nodes = dict()

    # Open SVG file
    xmldoc = minidom.parse(name + '.svg')
    gs = xmldoc.getElementsByTagName('g')
    for i in range(1, len(gs)):

        # Get group
        g = gs[i]

        # Take only nodes, not edges
        nid = str(g.getAttribute('id'))
        if nid.startswith('node'):

            # Title
            ti = g.getElementsByTagName('title')[0]
            name = str(ti.firstChild.nodeValue)

            # Scale
            SCALE = 20
            isellipse = False
            for el in g.childNodes:
                if not el.nodeType == el.TEXT_NODE:
                    if 'ellipse' in el.tagName:
                        # Information
                        cx = float(el.getAttribute('cx')) / SCALE
                        cy = float(el.getAttribute('cy')) / SCALE
                        rx = float(el.getAttribute('rx')) / SCALE
                        ry = float(el.getAttribute('ry')) / SCALE
                        isellipse = True

                        # Position
                        px = cx - rx
                        py = cy - ry
                        w = 2 * rx
                        h = 2 * ry

                    elif 'polygon' in el.tagName:
                        points = el.getAttribute('points')
                        points = points.split(' ')
                        xs = np.zeros(len(points))
                        ys = np.zeros(len(points))
                        for j in range(len(points)):
                            pt = points[j].split(',')
                            xs[j] = float(pt[0]) / SCALE
                            ys[j] = float(pt[1]) / SCALE
                        px = xs.min()
                        py = ys.min()
                        w = xs.max() - xs.min()
                        h = ys.max() - ys.min()
                        isellipse = False

            # Save for later linking
            nodes[name] = idx

            # Check if it is from outside
            styleid = 0
            for colla in COLLES:
                if name.startswith(colla):
                    styleid = 1
                    break

            # Write file
            if isellipse:
                line = ELLIPSE.format(idx)
            else:
                line = RECTANGLE.format(idx)
            diafh.write(line)
            line = POSITION.format(px, py, px, py, w, h, STYLES[styleid])
            diafh.write(line)
            line = TEXT.format(idx + 1, px, py, name, idx)
            diafh.write(line)
            idx += 2

    # Add links
    for key in graph.keys():
        for dep in graph[key]:

            # Add arrow link
            line = ARROW.format(idx, nodes[key], nodes[dep])
            diafh.write(line)
            idx += 1

    # Close file
    line = '\n  </dia:layer>\n</dia:diagram>'
    diafh.write(line)
    diafh.close()


# ==============================================================================
if __name__ == '__main__':

    if len(sys.argv) > 1:
        # Get the file
        fname = sys.argv[1]
        name = fname[:-4]
        ext = fname[-3:]
        assert(ext == 'dot')

        # Get and process the graph
        print("Getting graph and names...")
        graph = graph_from_dot(fname)
        names = names_from_dot(fname)
        print("Check file (manually check similar names)...\n")
        print('\n'.join(names))
        print('')
        if check_names(graph, names):  # TODO: check also name similarity
            print('Creating prelliminary SVG file...')
            os.system('{:s} -Tsvg {:s} > {:s}.svg'.format('fdp', fname, name))
            print('Creating Dia file...')
            dia_creator(name, graph)
            print('Done')
            # Delete temp files
            os.system('rm {:s}.svg'.format(name))
        else:
            print("\nSome errors found. No output file created.")

    else:
        # Show usage
        print("Usage:\n  {:s} file.dot".format(sys.argv[0]))
        print("Debug using:\n  fdp -Tpng example.dot -o example.png")
