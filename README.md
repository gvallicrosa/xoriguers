## Xoriguers: el codi per fer les xorimalles

### Installation

Just clone this repo:

    git clone https://bitbucket.org/gvallicrosa/xoriguers.git

### Requeriments

* dia (a graph editor)
* graphviz (the tool to generate graphs)
* python3 (to execute the algorithm)
* python3-numpy (math library)
* python3-matplotlib (OPTIONAL: to plot some statistics)
* python3-seaborn (OPTIONAL: nicer plots on top of matplotlib)

#### Ubuntu installation

    sudo apt install dia graphviz python3-numpy
    sudo apt install python3-matplotlib python3-seaborn

#### MacOS installation

    brew install python
    brew install graviz
    sudo pip3 install numpy
    brew cask install xquartz
    brew cask install dia

After his it won't run because DISPLAY=:0 env var is not set

    vim /Applications/Dia.app/Contents/Resources/bin/dia

Add the following content to line 40 (right before the oascript call)

    #########################################################
    # Ref: http://navkirats.blogspot.de/2014/10/dia-diagram-mac-osx-yosemite-fix-i-use.html
    versionOSX=$(sw_vers -productVersion | awk -F '.' '{print $(NF-1)}')
    [[ ${versionOSX} -ge 10 ]] && export DISPLAY=:0
    #########################################################

### Tutorial [malla_generator.py]

Take the template file `example.dot` and fill it with the info provided by `lanxoveta@gmail.com` as `name1 -- name2` at the bottom of the file. Remove the example entries.

You can see an example graph using:

    fdp -Tpng example.dot -o example.png

Now execute the `malla_generator` using:

    ./malla_generator.py example.dot

This script will first check that you defined all the possible names of the pairs inside one of the blocks [nois, noies, nois altres, noies altres] and that there are no unused names on those blocks.

You should correct the errors and also check that the **same person does not have two versions of the same name** in the list (ex. accents).

Once everything is corrected you can launch again the `malla_generator.py`. We corrected the errors and putted them in `example_corrected.dot`, so now:

    mv example_corrected.dot example.dot
    ./malla_generator.py example.dot

Now it will generate a `example.dia` file which is an editable graph. Open it with `dia` program. Once inside, select all the graph and move it a bit, this will put the text in the correct place.

Manually adjust the positions of the nodes until you are happy enough and export it as png image.

### Tutorial [malla_statistics.py]

double check names

### Troubleshooting

If running `fdp -Tpng example.dot -o example.png` gives an error, you have a bad formatted dot file.
